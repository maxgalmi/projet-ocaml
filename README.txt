Projet Programmation Fonctionnelle Avancée

Sujet: plus court chement entre deux points extraits à partir d'un bout de carte openstreetmap

Configuration
-------------
*OCaml version:  OCaml toplevel, version 4.01.0
*Xml-light: version 2.4



Running
-------
Pour lancer le programme:

    *compilation: il suffit d'excuter le fichier build.sh 
    
    *lancement: pour lancer le programme il faut taper le nom programme (./projet) avec le nom de la carte.
	exemple: ./projet paris_sudest.osm
    
    *Erreur lié au fichier: 
	  le programme vous signale si vous oubliez de donner en argument le nom de la carte 
	  ou si le nom ne correspondant pas à un fichier existant.

Entré
-----
Le programme prend en Entré comme mentionné ci-dessus le nom d'un fichier osm qui une partie d'une carte openstreetmap.


Sortie
------
A la fin de la recherche du chemin, le pragramme génére un fichier "result.gpx" qui permet de visualer le chemin
grâce aux différents gpx viewer. Nous utilisons celui disponible à l'adresse suivante: http://maplorer.com/view_gpx_fr.html
Il vous siffira alors qu'à charger le fichier result.gpx pour visualiser le chemin (en couleur) sur une carte...