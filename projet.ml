(*#directory "+xml-light";;
  #load "xml-light.cma";;
  #load "unix.cma";;
*)
open Xml;;

type node = Node of int * float * float * (tag list)
and way = Way of int * int list * tag list
and area = way
and tag = Tag of string * string

(* let file = "paris_sudest.osm";; *)


let rec get_filename () = 
  try
    let x = Sys.argv.(1) in 
    try  
      let y = open_in x 
      in x 
    with Sys_error _ -> print_endline "fichier inexistant!"; exit 0
  with Invalid_argument("index out of bounds") -> 
    print_endline "Nom de la carte" ; 
    exit 0 ;;
 
let file = get_filename () ;;


let fst = function (x,_,_,_,_,_) -> x;;
let snd = function (_,y,_,_,_,_) -> y;;
let thd = function (_,_,z,_,_,_) -> z;;
let fth = function (_,_,_,a,_,_) -> a;;
let ffth = function (_,_,_,_,b,_) -> b;;
let sth = function (_,_,_,_,_,c) -> c;;

let rec xml_list_to_tag_list l = match l with
  |[]-> []
  |Element("tag",[("k",x);("v",y)],_)::reste -> Tag(x,y)::(xml_list_to_tag_list reste)
  |_::reste -> xml_list_to_tag_list reste;;

let rec xml_list_to_nd_list l = match l with
  |[]-> []
  |Element("nd",[("ref",x)],_)::reste -> (int_of_string x)::(xml_list_to_nd_list reste)
  |_::reste -> xml_list_to_nd_list reste;;


let xml_to_node = function
  |Element("node",("id",x)::("lat",lat)::("lon",long)::_,tags) -> Node(int_of_string x,float_of_string lat,float_of_string long,xml_list_to_tag_list tags)
  |_ -> failwith "Node expected";;

let xml_to_way = function
  |Element("way",("id",x)::_,nd_tags) -> Way(int_of_string x,xml_list_to_nd_list nd_tags,xml_list_to_tag_list nd_tags)
  |_ -> failwith "Way expected";;

let is_area = function
  |Way(_,x,_)-> (List.nth x 0) = (List.nth x ((List.length x)-1));;

let xml_to_lists = function
  |Element ("osm",_,xml)-> let rec aux xml node_list way_list area_list = match xml with
    |[]-> (node_list,way_list,area_list)
    |Element("node",x,y)::reste-> let n = Element("node",x,y)
				  in aux reste ((xml_to_node n)::node_list) way_list area_list
    |Element("way",x,y)::reste -> let w = Element("way",x,y) in let w2 = xml_to_way w in
								if is_area w2 then aux reste node_list way_list (w2::area_list)
								else aux reste node_list (w2::way_list) area_list
    |_::reste -> aux reste node_list way_list area_list
			   in aux xml [] [] []
  |_-> failwith "osm expected";;


let rec node_search node_list id = match node_list with
  |[]-> raise (Invalid_argument ("Node not found"^(string_of_int id)))
  |Node(i,lat,lon,tags)::reste -> if id = i then Node(i,lat,lon,tags) else node_search reste id ;;

let rec way_search id = function
  |[]-> failwith "Way not found"
  |Way(i,ids,tags)::reste -> if id = i then Way(i,ids,tags) else way_search id reste;;

let get_node_id = function
  |Node(id,_,_,_)-> id;;
let get_way_id = function
  |Way(id,_,_)-> id;;


let pi = 3.14159265358979323;;
let deg2rad x = x *. pi /. 180.;;
let earth_radius = 6378137.;;
let get_distance x y = match (x,y) with
  |Node(_,lat_x,lon_x,_),Node(_,lat_y,lon_y,_)->
    let lat_x = deg2rad lat_x and lon_x = deg2rad lon_x
    and lat_y = deg2rad lat_y and lon_y = deg2rad lon_y
    in let dlo = (lon_y -. lon_x)/.2. and dla = (lat_y-.lat_x)/.2.
       in let a = (sin dla *. sin dla) +. cos lat_x *. cos lat_y *. (sin dlo *. sin dlo)
	  in let d = 2.0 *. (atan2 (sqrt a) (sqrt (1.-.a)))
	     in earth_radius *. d;;


let get_way_tags = function
  |Way(_,_,tags)-> tags;;
let get_ids = function
  |Way(_,ids,_)-> ids;;
let rec tags_to_list = function
  |[]-> []
  |(Tag(x,y))::l ->(x,y)::(tags_to_list l);;


let rec highway = function |[]->""
  |Tag("highway",x)::reste -> x
  |_::reste -> highway reste;;

let rec maxspeed = function |[]-> 0.
  |Tag("maxspeed",x)::reste -> 
    (try 
       float_of_string x
     with Failure("float_of_string") -> match x with
     | "FR:urban" -> 50.0
     | _ -> print_string x; 0.000 )
  |_::reste -> maxspeed reste;;


let is_traffic_signals= function
  |Node(_,_,_,tags) ->(highway tags) = "traffic_signals" ;;


let way_highway w= highway (get_way_tags w);;

let is_roundabout w = let rec is_roundabout_aux = begin function
  |[]->false
  |Tag("junction","roundabout")::reste -> true
  |_::reste -> is_roundabout_aux reste end in
		      is_roundabout_aux (get_way_tags w);;


let vitesse w = let tags = get_way_tags w in
		let x = maxspeed tags in if x <> 0. then (x -. 5.)
		  else if is_roundabout w then 20.
		  else
		    let h = highway tags in match h with
		      | "motorway" -> 120.
		      | "trunk" -> 80.
		      | _-> 40.
;;

let get_temps distance chemin = function
  |"vehicle" -> distance /.((vitesse chemin)*. (1000./.60.)) (*en minutes*)
  |"bicycle" -> distance /.((16.)*. (1000./.60.))
  |"pedestrian" -> distance /.((5.)*. (1000./.60.))
  |_->raise (Invalid_argument "");;
let no_all = ["bus_guideway";"raceway"];; (*chemins interdits � tous*)
let no_vehicle = ["footway";"cycleway";"bridleway";"steps"];; (*chemins interdits aux vehicules*)
let no_pedestrian = ["motorway";"trunk"];;
let no_bicycle = ["motorway";"trunk"];;

let is_vehicle_way w =
  let highway = way_highway w in
  not(List.mem ("motor_vehicle","no") (tags_to_list (get_way_tags w)))&&((List.mem ("motor_vehicle","yes") (tags_to_list (get_way_tags w)))||(not((List.mem highway no_all)||(List.mem highway no_vehicle))));;

let is_bicycle_way w =
  let highway = way_highway w in
  not(List.mem ("bicycle","no") (tags_to_list (get_way_tags w)))&&((List.mem ("bicycle","yes") (tags_to_list (get_way_tags w)))||(not((List.mem highway no_all)||(List.mem highway no_bicycle))));;

let is_pedestrian_way w =
  let highway = way_highway w in
  not(List.mem ("foot","no") (tags_to_list (get_way_tags w)))&&((List.mem ("foot","yes") (tags_to_list (get_way_tags w)))||(not((List.mem highway no_all)||(List.mem highway no_pedestrian))));;

let is_oneway w =
  List.mem ("oneway","yes") (tags_to_list (get_way_tags w));;

let rec possibles_ways id mode = function
  |[]->[]
  |Way(x,ids,tags)::reste -> let w = Way(x,ids,tags) in if (List.mem id ids) then begin
    if (mode="vehicle")&&(is_vehicle_way w) then w::(possibles_ways id mode reste)
    else if (mode="bicycle")&&(is_bicycle_way w) then w::(possibles_ways id mode reste)
    else if (mode="pedestrian")&&(is_pedestrian_way w) then w::(possibles_ways id mode reste)
    else if (mode="") then w::(possibles_ways id mode reste)
    else (possibles_ways id mode reste) end
    else possibles_ways id mode reste;;



let rec mem a l = match l with
  |[]-> -1
  |x::l' -> if x=a then 0 else 1+ (mem a l');; (*prend en argument un element et une liste et si cette �l�ment appartient � la liste, renvoie sa position*)

let possibles_nodes_aux id w mode =
  let ids = get_ids w in
  let length = List.length ids in
  let n = mem id ids in
  if n=0 then [List.nth ids 1,Some w]
  else if n=length-1 then begin
    if (is_oneway w)&&(mode="vehicle") then [] else [(List.nth ids(length -2),Some w)] end
  else if (is_oneway w)&&(mode="vehicle") then [(List.nth ids (n+1),Some w)] else [List.nth ids (n-1),Some w;List.nth ids (n+1),Some w];;

let rec deleteDuplicate l =
  match l with
  | [] -> []
  | x :: rest ->
    if List.mem x rest then deleteDuplicate (rest)
    else x :: deleteDuplicate ( rest) ;;
let rec possibles_nodes id way_list mode = match way_list with
  |[]->[]
  |w::l'-> deleteDuplicate (possibles_nodes_aux id w mode)@(possibles_nodes id l' mode);;



let get_node_way_list node_list way_list =
  let rec list = begin function
    |[]->[]
    |w::rest -> (get_ids w)@(list rest) end
  in let l =list way_list in
     let l' = deleteDuplicate l in
     List.map (node_search node_list) l';;


let (node_list,way_list,area_list,node_way_list) =
  try (* Si le fichier marshall� existe, on le demarshalle*)
    let y = open_in (file ^ ".donnees.txt")
    in let z = Marshal.from_channel y in (z:(node list*way list* way list * node list))
  with Sys_error _ -> (*Sinon on parse le fichier xml puis on le marshalle dans un fichier txt pour le r�utiliser les fois suivantes*)
    print_endline "Creation du fichier de donnees veuillez patientez";
    let y = parse_file file in
    let (node_list,way_list,area_list) = xml_to_lists y in
    let node_way_list = get_node_way_list node_list way_list in
    let z = open_out (file ^ ".donnees.txt") in
    Marshal.to_channel z (node_list,way_list,area_list,node_way_list) [];
    close_out z; (node_list,way_list,area_list,node_way_list) ;;


let no_option = function
  |None -> raise (Invalid_argument "'a option expected")
  |Some w -> w;;

(*cree une liste de quadruplets (id_noeud,id_predecesseur,distance au noeud de d�part, temps au noeud de d�part , chemin sur lequel se trouve le noeud, qualit� du noeud ) � partir d'une liste d'ids*)
let rec creer_sixuplets node_arrivee predecesseur id_list mode = 
  match id_list with
  | [] -> []
  | (id_noeud,chemin)::rest -> 
    let n1 = node_search node_list id_noeud
    and n2 = node_search node_list (fst predecesseur) in
    let distance1 = get_distance n1 n2 in
    let distance = distance1 +.(thd predecesseur) (*distance au noeud de d�part = distance du noeud au predecesseur + distance du predecesseur au noeud de d�part*)
    in let temps = get_temps (distance1) (no_option chemin) mode +. fth predecesseur
       in let temps = if is_traffic_signals n2 then temps +. 1. else temps
	  in let qualite = distance1 +. get_distance n1 node_arrivee
	     in (id_noeud, fst predecesseur,distance,temps,chemin,qualite)::(creer_sixuplets node_arrivee predecesseur rest mode);;


let min_qual x y = match(x,y) with
  |(_,_,_,_,_,xqual),(_,_,_,_,_,yqual) -> if xqual > yqual then y else x;;


let rec trouve_min = function
  | [] -> raise (Invalid_argument "liste vide")
  | [x] -> x
  | x::r -> min_qual (trouve_min r) x ;;


let rec supprimer_element e = function
  |[]->[]
  |x::l' -> if x = e then (supprimer_element e l') else x::(supprimer_element e l');;


(*Prend en argument deux listes et supprime de la premi�re liste les elements appartenants � la seconde*)
let rec supprimer_element2 l = function
  |[]-> l
  |x::l' -> let rec supprimer_element e = function
    |[]->[]
    |x::l' -> if Pervasives.fst x = Pervasives.fst e then (supprimer_element e l') else x::(supprimer_element e l') in

	    supprimer_element2 (supprimer_element x l) l';;



(*Supprime l'element ayant la plus petite qualit� de ns et l'ajoute � nc*)
let rec ajoute nc ns = let e = trouve_min ns in (e::nc,supprimer_element e ns);;

let rec final_list_aux x l =
  if fst x = snd x then [fst x,ffth x] else
    match l with
    |[]-> failwith "Aucun chemin trouv�"
    |_::_ -> let p a = (snd x = fst a) in
	     let a = List.find p l in
	     let l' = supprimer_element a l in
	     (final_list_aux a l')@[fst x,ffth x] ;;	


let final_list = function
  | [] -> failwith "Aucun chemin trouv�"
  | x::l' -> (final_list_aux x l'),(thd x),(fth x);;


let rec algo_recherche_aux node_arrivee mode nc ns =

  print_endline (string_of_int (List.length nc));
  let ids_nc = List.combine (List.map fst nc) (List.map ffth nc) in
  if (List.mem (get_node_id node_arrivee) (Pervasives.fst (List.split ids_nc)))
  then nc 
  else begin
    (*Le dernier element ajout� est le premier de la liste, car on ajoute les elements par la gauche*)
    let predecesseur = List.nth nc 0 in 
    let id = fst predecesseur in
    let pw = possibles_ways id mode way_list in
    let pn = possibles_nodes id pw mode in
    let id_suivants = supprimer_element2 pn ids_nc in
    let suivants = creer_sixuplets node_arrivee predecesseur id_suivants mode in
    let ns= deleteDuplicate (ns@suivants) in
    let x = try
	      let (nc,ns) = ajoute nc ns in
	      algo_recherche_aux node_arrivee mode nc ns
      with Invalid_argument("liste vide") -> print_endline "Interruption"; 
	nc in 
    x 
  end
;;

let algo_recherche id_depart id_arrivee mode =
  let nc = [(id_depart, id_depart, 0., 0., None, 0.)] and ns = [] in
  algo_recherche_aux (node_search node_list id_arrivee) mode nc ns;;

(*let node_way_list = [Node(1,1.,1.,[])];;*)

let id_way_to_gpx node_list = function
  | (id,w) ->
    let node_id = node_search node_way_list id in
    match node_id with
    | Node(_,lat,lon,_)-> try
			   "<wpt lat=\"" ^(string_of_float lat) ^"\" lon=\"" ^(string_of_float lon) ^"\"/> <!-- node id : " ^
			     string_of_int id ^ " way id : " ^ string_of_int (get_way_id (no_option w)) ^" --> \n"
      with (Invalid_argument "'a option expected") ->
	"<wpt lat=\"" ^(string_of_float lat) ^"\" lon=\"" ^(string_of_float lon) ^"\"/> <!-- node id : " ^
	  string_of_int id ^ " --> \n";;

let rec id_way_list_to_gpx node_list = (function
  |[]-> ""
  |x::rest -> (id_way_to_gpx node_list x) ^ (id_way_list_to_gpx node_list rest))
and result_to_gpx node_list = function
  |(id_way_list,distance,temps)->
    "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> \n"
    ^ "<gpx version=\"1.1\"> \n" ^ id_way_list_to_gpx node_list id_way_list ^ "</gpx> <!-- Distance a parcourir : " ^
      string_of_float distance ^ " m Temps estime : " ^ string_of_float temps ^" min --> \n" ;;

let min_distance n1 n2 n3 =
  if (get_distance n1 n2) < (get_distance n1 n3) then n2 else n3;;


let plus_proche node node_list =
  let rec list_distance n node_list list_result = 
    begin match node_list with
    | [] -> list_result
    | x :: rest -> (*print_endline (string_of_int (List.length node_list));*) 
      if x <> n then (list_distance n rest [x,(get_distance n x)]@list_result) 
      else
	(list_distance n rest list_result)
    end
  and trouve_min2 l = 
    begin
      let min2 (x,y) (x',y') = if y<y' then (x,y) else (x',y') in
      match l with
      | []-> failwith "liste vide"
      | [x,y] -> (x,y)
      | (x,y) :: rest -> min2 (trouve_min2 rest) (x,y) 
    end 
  in
  trouve_min2 (list_distance node node_way_list []);;


let filename = "res.xml"

(**let get_id_of_addr xml = match xml with
  |Element("searchresults",_,a::l) ->
    match a with
    |Element("place",_::("osm_type","node")::("osm_id",id)::_,_)-> int_of_string id
    |Element("place",_::("osm_type","way")::("osm_id",id)::_,_)-> match way_search (int_of_string id) (way_list@area_list) with
      |Way(_,idn::_,_)-> idn;;
*)

let get_id_of_addr xml = match xml with
  | Element ("searchresults", _, a :: l) ->  (
    match a with
    | Element ("place", _ :: ("osm_type", "node") :: ("osm_id", id) :: _, _) -> 
      int_of_string id
    | Element ("place", _ :: ("osm_type","way") :: ("osm_id",id) :: _, _) -> 
      (
	try
	  match way_search (int_of_string id) (way_list@area_list) with
	  | Way (_, idn :: _, _) -> idn
	  | Way (_, [], _) -> -1
	with Failure ("Way not found") -> -1 
      )
    | Element ("place", _, _) -> -1 
    | _ -> -1
  )
  | PCData _ -> -1
  | _ -> -1
;;

let lire_addr_aux msg filename =
  Printf.printf msg;
  let cmd = "wget -q -O " ^ filename ^ " 'http://nominatim.openstreetmap.org/search?format=xml&addressdetail=1&q=" and
      addr = read_line() in
  print_endline (cmd^addr^"'");
  let _ = Sys.command (cmd^addr^"'") in
  let xml = parse_file filename
  in get_id_of_addr xml;;
(* let x = parse_file filename;;*)
(*lire_addr "adresse : " filename;;*)

let c = ref 0;; (* compteur pour le nom des fichier pgx de sortie ... *)

let rec lire_addr msg filename = 
  let x = lire_addr_aux msg filename in
  if x < 0 then  (
    print_endline "Erreur dans l'adresse, reessayez:"; 
    lire_addr msg filename ) 
  else x 

let verify_prompt () = Unix.isatty Unix.stdin && Unix.isatty Unix.stdout ;;


let rec prompt () =
  while true do
    let id_depart = lire_addr "Entrez l'adresse de d�part :" filename and
	id_arrivee = lire_addr "Entrez l'adresse d'arriv�e :" filename and
	mode = 
      let rec choix_mode () = 
	begin 
	  print_endline "Mode de transport : 1. Pieton 2. V�lo 3. Voiture";
	  let m = try
		    match read_int() with
		    | 1 -> "pedestrian"
		    | 2 -> "bicycle"
		    | 3 -> "vehicle"
		    | _ -> print_endline "Chiffre non valide."; 
		      choix_mode ()
	    with
	      Failure "int_of_string" -> print_endline "Entrez un entier"; 
		choix_mode () in m 
	end
      in choix_mode() in

    let id_depart =
      if (possibles_ways id_depart "" way_list) = [] then
	let node_depart = node_search node_list id_depart in 
	print_endline "Calcul du point le plus proche du point de d�part";
	get_node_id (Pervasives.fst (plus_proche node_depart node_way_list)) 
      else
	id_depart in
    let id_arrivee =
      if (possibles_ways id_arrivee "" way_list) = [] then
	let node_arrivee = node_search node_list id_arrivee in 
	print_endline "Calcul du point le plus proche du point d'arriv�e";
	get_node_id (Pervasives.fst (plus_proche node_arrivee node_way_list)) 
      else
	id_arrivee in
    print_endline "Calcul du plus court chemin";
    let nc = algo_recherche id_depart id_arrivee mode in
    let result = final_list nc in
    let gpx = result_to_gpx node_list result in
    c := !c+1; 
    let z = open_out ("result_" ^ string_of_int !c ^ ".gpx") in
    output_string z gpx;
    close_out z;
    print_endline "Voulez continuer? [O\\n]" ;
    match input_char stdin with
    | 'n' -> exit 0
    | _ -> prompt()
  done  
in 
prompt ()
;;
